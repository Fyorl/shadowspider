This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Dependencies

* [Maven 3+](https://maven.apache.org/download.cgi)
* [Java 8+](http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html)

### Building

```
$ mvn clean install
```

### Running

```
$ java -jar target/shadowspider.jar
```

Supply the `--help` argument for a list of options though running the program without any options will use sensible defaults. There is no proxy support so it cannot be run behind a corporate proxy without first tunneling out.
