package org.bytestruct.shadowspider;

import org.json.JSONObject;
import org.junit.Test;
import se.softhouse.jargo.CommandLineParser;
import se.softhouse.jargo.ParsedArguments;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ShadowSpiderTest {
	@SuppressWarnings("ConstantConditions")
	private static String readResourceToFile (@Nonnull final String resource) {
		final StringBuilder builder = new StringBuilder();
		try (final Reader reader =
			new InputStreamReader(
				ShadowSpiderTest.class.getResourceAsStream(resource)
				, StandardCharsets.UTF_8))
		{
			final char[] buf = new char[8192];
			int n;

			while ((n = reader.read(buf)) > -1) {
				builder.append(buf, 0, n);
			}
		} catch (final IOException e) {
			assertNull(e);
		}

		return builder.toString();
	}

	private ParsedArguments args () {
		return CommandLineParser.withArguments(Options.timeout).parse();
	}

	@Test
	public void testFollower () {
		final ShadowSpider spider =	new ShadowSpider(args());
		final JSONObject json = new JSONObject();
		final String contents = readResourceToFile("/follower.html");
		spider.extractMetadata(json, contents);

		assertEquals("BLOODCRAFT", json.getString("craft"));
		assertEquals("Follower", json.getString("type"));
		assertEquals("Bronze", json.getString("rarity"));
		assertEquals("Standard Card Pack", json.getString("set"));
		assertEquals(1, json.getInt("cost"));

		final JSONObject base = json.getJSONObject("base");
		final JSONObject evolved = json.getJSONObject("evolved");

		assertEquals(1, base.getInt("atk"));
		assertEquals(2, base.getInt("def"));
		assertEquals("Fanfare: Deal 1 damage to both leaders.", base.getString("effect"));
		assertEquals(
			"A shadow born from human hatred. This parasite warps the spirit and then feeds on it, "
			+ "sapping life force to boost its own dark power."
			, base.getString("flair"));

		assertEquals(3, evolved.getInt("atk"));
		assertEquals(4, evolved.getInt("def"));
		assertEquals(
			"As long as hatred remains in the world, dark shadows will grow ever longer and more "
			+ "powerful. They will one day manifest as a pure terror condemning all light and hope "
			+ "to the darkness."
			, evolved.getString("flair"));
	}

	@Test
	public void testSpell () {
		final ShadowSpider spider = new ShadowSpider(args());
		final JSONObject json = new JSONObject();
		final String contents = readResourceToFile("/spell.html");
		spider.extractMetadata(json, contents);

		assertEquals("NEUTRAL", json.getString("craft"));
		assertEquals("Spell", json.getString("type"));
		assertEquals("Silver", json.getString("rarity"));
		assertEquals("Standard Card Pack", json.getString("set"));
		assertEquals(1, json.getInt("cost"));

		final JSONObject base = json.getJSONObject("base");
		assertEquals("Deal 1 damage to an enemy.", base.getString("effect"));
		assertEquals(
			"One loosed arrow becomes a light that pierces the heart of an evildoer."
			, base.getString("flair"));
	}

	@Test
	public void testAmulet () {
		final ShadowSpider spider = new ShadowSpider(args());
		final JSONObject json = new JSONObject();
		final String contents = readResourceToFile("/amulet.html");
		spider.extractMetadata(json, contents);

		assertEquals("BLOODCRAFT", json.getString("craft"));
		assertEquals("Amulet", json.getString("type"));
		assertEquals("Silver", json.getString("rarity"));
		assertEquals("Standard Card Pack", json.getString("set"));
		assertEquals(1, json.getInt("cost"));

		final JSONObject base = json.getJSONObject("base");
		assertEquals(
			"Countdown (4)\nAt the end of your turn, deal 1 damage to both leaders."
			, base.getString("effect"));
		assertEquals(
			"The roses of the underworld are dyed with fresh blood."
			, base.getString("flair"));
	}

	@Test
	public void testApplyPattern () {
		final String haystack = readResourceToFile("/all-cards.html");
		final List<String> cardIDs = ShadowSpider.applyPattern(ShadowSpider.linkMatcher, haystack);
		final List<String> imgIDs =
			ShadowSpider.applyPattern(ShadowSpider.smallCardMatcher, haystack);

		assertEquals(888, cardIDs.size());
		assertEquals(888, imgIDs.size());
	}
}
