package org.bytestruct.logging;

import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

public class Logger {
	private final org.slf4j.Logger delegate;

	private Logger (@Nonnull final org.slf4j.Logger delegate) {
		this.delegate = delegate;
	}

	@Nonnull
	public static Logger getLogger (@Nonnull final Class<?> cls) {
		return new Logger(LoggerFactory.getLogger(cls));
	}

	public void info (@Nonnull final String msg, @Nonnull final Object... args) {
		delegate.info(String.format(msg, args));
	}

	public void debug (@Nonnull final String msg, @Nonnull final Object... args) {
		delegate.debug(String.format(msg, args));
	}

	public void warn (@Nonnull final String msg, @Nonnull final Object... args) {
		delegate.warn(String.format(msg, args));
	}

	public void error (@Nonnull final String msg, @Nonnull final Object... args) {
		delegate.error(String.format(msg, args));
	}

	public void error (
		@Nonnull final String msg
		, @Nonnull final Throwable e
		, @Nonnull final Object... args)
	{
		delegate.error(String.format(msg, args), e);
	}

	public void trace (@Nonnull final String msg, @Nonnull final Object... args) {
		delegate.trace(String.format(msg, args));
	}
}
