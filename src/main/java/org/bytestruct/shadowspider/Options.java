package org.bytestruct.shadowspider;

import com.google.common.collect.Range;
import se.softhouse.jargo.Argument;
import se.softhouse.jargo.Arguments;

import java.io.File;

class Options {
	private static final int DEFAULT_DELAY = 500;
	private static final int DEFAULT_TIMEOUT = 5000;
	static final File DEFAULT_DEST = new File("dump");

	static final Argument<?> help = Arguments.helpArgument("-h", "--help");

	static final Argument<Integer> delay =
		Arguments.integerArgument("--delay")
			.defaultValue(DEFAULT_DELAY)
			.limitTo(Range.atLeast(0))
			.description("The number of ms between each server request to avoid spamming/DoS.")
			.build();

	static final Argument<Integer> timeout =
		Arguments.integerArgument("--timeout")
			.defaultValue(DEFAULT_TIMEOUT)
			.limitTo(Range.atLeast(0))
			.description("HTTP request timeout.")
			.build();

	static final Argument<File> dest =
		Arguments.fileArgument("--dest")
			.defaultValue(DEFAULT_DEST)
			.description("The location to place the downloaded images.")
			.build();
}
