package org.bytestruct.shadowspider;

import com.google.common.annotations.VisibleForTesting;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;
import org.bytestruct.logging.Logger;
import org.json.JSONObject;
import org.json.JSONTokener;
import se.softhouse.jargo.ArgumentException;
import se.softhouse.jargo.CommandLineParser;
import se.softhouse.jargo.ParsedArguments;

import javax.annotation.Nonnull;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ShadowSpider {
	private static final Logger logger = Logger.getLogger(ShadowSpider.class);
	@VisibleForTesting static final Pattern linkMatcher =
		Pattern.compile("<a [^>]*?href=\"/cards/([^\"]+)");
	@VisibleForTesting static final Pattern smallCardMatcher =
		Pattern.compile("<img [^>]*?data-original=\"/images/sv/smallCard/en/([^\"]+)\\.png");

	private final HttpClient client;
	private final ParsedArguments opts;

	public static void main (@Nonnull final String[] args) {
		try {
			final ParsedArguments opts =
				CommandLineParser
					.withArguments(Options.delay, Options.timeout, Options.dest)
					.andArguments(Options.help)
					.parse(args);
			final ShadowSpider spider = new ShadowSpider(opts);
			spider.spider();
		} catch (final ArgumentException e) {
			System.out.printf("%s%n", e.getMessageAndUsage());
			System.exit(1);
		}
	}

	private static Optional<Integer> parseInt (@Nonnull final Optional<String> s) {
		if (!s.isPresent()) {
			return Optional.empty();
		}

		return parseInt(s.get());
	}

	private static Optional<Integer> parseInt (@Nonnull final String s) {
		final NumberFormat intFormat = NumberFormat.getInstance(Locale.UK);
		intFormat.setParseIntegerOnly(true);
		final ParsePosition pos = new ParsePosition(0);
		final Number n = intFormat.parse(s, pos);

		if (pos.getIndex() == s.length()) {
			return Optional.of(n.intValue());
		} else {
			logger.error("Failed to convert '%s' to a number.", s);
			return Optional.empty();
		}
	}

	@VisibleForTesting
	ShadowSpider (@Nonnull final ParsedArguments opts) {
		logger.info("Spider initialising...");
		this.opts = opts;
		final Integer timeout = opts.get(Options.timeout);
		assert timeout != null;

		final RequestConfig config =
			RequestConfig.custom()
				.setConnectionRequestTimeout(timeout)
				.setCookieSpec(CookieSpecs.STANDARD)
				.build();
		final HttpClientBuilder builder = HttpClientBuilder.create();
		builder.setDefaultRequestConfig(config);
		client = builder.build();
	}

	private void spider () {
		logger.info("Spider starting...");

		try {
			final HttpGet get = new HttpGet("http://sv.bagoum.com/cardSort");
			final HttpResponse res = client.execute(get);
			final HttpEntity entity = res.getEntity();
			final ContentType contentType = ContentType.getOrDefault(entity);

			try (final Reader reader =
				new InputStreamReader(entity.getContent(), contentType.getCharset()))
			{
				int n;
				final char[] buf = new char[8192];
				final StringBuilder contents = new StringBuilder();

				while ((n = reader.read(buf)) > -1) {
					contents.append(buf, 0, n);
				}

				logger.info("Retrieved full card list, processing...");
				final String haystack = contents.toString();
				final List<String> cardIDs = applyPattern(linkMatcher, haystack);
				final List<String> imgIDs =
					cardIDs.stream().map(String::toLowerCase).collect(Collectors.toList());
				downloadMetadata(cardIDs);
				downloadImages(imgIDs);
			}
		} catch (final IOException | NullPointerException e) {
			logger.error("HTTP request failed: %s", e.getMessage());
		}

		logger.info("Spider finished...");
	}

	@Nonnull
	@VisibleForTesting
	static List<String> applyPattern (
		@Nonnull final Pattern pattern
		, @Nonnull final String haystack)
	{
		final List<String> ids = new ArrayList<>();
		final Matcher matcher = pattern.matcher(haystack);

		while (matcher.find()) {
			ids.add(matcher.group(1).replaceAll("[ ,']", ""));
		}

		return ids;
	}

	private void downloadMetadata (@Nonnull final List<String> ids) {
		final Integer delay = opts.get(Options.delay);
		final File dest = getDest();
		assert delay != null;

		final File manifest = new File(dest, "manifest.json");
		JSONObject json;

		if (manifest.exists()) {
			try {
				json =
					new JSONObject(
						new JSONTokener(
							new InputStreamReader(
								new FileInputStream(manifest)
								, StandardCharsets.UTF_8)));
			} catch (final FileNotFoundException ignored) {
				json = new JSONObject();
			}
		} else {
			json = new JSONObject();
		}

		try (final Writer writer =
			new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(manifest), StandardCharsets.UTF_8)))
		{
			for (final String id : ids) {
				if (json.has(id)) {
					logger.info("Already have metadata for '%s', skipping...", id);
					continue;
				}

				final HttpGet get = new HttpGet(String.format("http://sv.bagoum.com/cards/%s", id));
				try {
					final HttpResponse res = client.execute(get);
					if (res.getStatusLine().getStatusCode() == 200) {
						logger.info("Processing metadata for %s...", id);
						final HttpEntity entity = res.getEntity();
						final long len = entity.getContentLength();
						final ByteArrayOutputStream out;
						if (len > -1 && len <= Integer.MAX_VALUE) {
							out = new ByteArrayOutputStream((int) len);
						} else {
							out = new ByteArrayOutputStream();
						}
						entity.writeTo(out);
						final String contents;
						if (entity.getContentEncoding() != null
							&& entity.getContentEncoding().getValue() != null)
						{
							contents = out.toString(entity.getContentEncoding().getValue());
						} else {
							contents = out.toString();
						}

						final JSONObject datum = new JSONObject();
						json.put(id, datum);
						extractMetadata(datum, contents);
					} else {
						logger.error("Failed to request metadata for %s...", id);
					}
				} catch (final IOException e) {
					logger.error(
						"Failed to download request metadata for %s: %s"
						, id
						, e.getMessage());
				} finally {
					get.releaseConnection();
				}
				try { Thread.sleep(delay); } catch (final InterruptedException ignored) {}
			}

			logger.info("Writing manifest...");
			json.write(writer);
		} catch (final IOException e) {
			logger.error("Failed to write to '%s': %s", manifest.getAbsolutePath(), e.getMessage());
		}
	}

	private static final Pattern craftMatcher =
		Pattern.compile("<div [^>]*?id=\"mainbody-intro\"[^>]*>[^<]+<span[^>]+>([^<]+)");
	private static final Pattern typeMatcher = Pattern.compile("<b>Type:</b>([^<]+)");
	private static final Pattern rarityMatcher = Pattern.compile("<b>Rarity:</b>([^<]+)");
	private static final Pattern setMatcher = Pattern.compile("<b>Set:</b>([^<]+)");
	private static final Pattern costMatcher = Pattern.compile("<b>Cost:</b>([^<]+)");
	private static final Pattern statsMatcher = Pattern.compile("<b>Stats:</b>([^<]+)");
	private static final Pattern effectMatcher = Pattern.compile("<b>Effect:</b>([^<]+)");
	private static final Pattern evolvedMatcher = Pattern.compile("<b>Evolved:</b>");
	private static final Pattern baseFlairMatcher =
		Pattern.compile("<b>Base Flair</b>[^<]*<br>([^<]+)");
	private static final Pattern evolvedFlairMatcher =
		Pattern.compile("<b>Evolved Flair</b>[^<]*<br>([^<]+)");

	@VisibleForTesting
	@SuppressWarnings("Duplicates")
	void extractMetadata (@Nonnull final JSONObject json, @Nonnull final String contents) {
		Optional<String> craft = Optional.empty();
		Optional<String> type = Optional.empty();
		Optional<String> rarity = Optional.empty();
		Optional<String> set = Optional.empty();
		Optional<String> cost = Optional.empty();
		Optional<String> baseStats = Optional.empty();
		Optional<String> baseEffect = Optional.empty();
		Optional<String> evolvedStats = Optional.empty();
		Optional<String> evolvedEffect = Optional.empty();
		Optional<String> baseFlair = Optional.empty();
		Optional<String> evolvedFlair = Optional.empty();

		final Matcher craftMatcher = ShadowSpider.craftMatcher.matcher(contents);
		final Matcher typeMatcher = ShadowSpider.typeMatcher.matcher(contents);
		final Matcher rarityMatcher = ShadowSpider.rarityMatcher.matcher(contents);
		final Matcher setMatcher = ShadowSpider.setMatcher.matcher(contents);
		final Matcher costMatcher = ShadowSpider.costMatcher.matcher(contents);
		final Matcher statsMatcher = ShadowSpider.statsMatcher.matcher(contents);
		final Matcher effectMatcher = ShadowSpider.effectMatcher.matcher(contents);
		final Matcher evolvedMatcher = ShadowSpider.evolvedMatcher.matcher(contents);
		final Matcher baseFlairMatcher = ShadowSpider.baseFlairMatcher.matcher(contents);
		final Matcher evolvedFlairMatcher = ShadowSpider.evolvedFlairMatcher.matcher(contents);

		if (craftMatcher.find()) {
			craft = Optional.of(craftMatcher.group(1).trim());
		}

		if (typeMatcher.find()) {
			type = Optional.of(typeMatcher.group(1).trim());
		}

		if (rarityMatcher.find()) {
			rarity = Optional.of(rarityMatcher.group(1).trim());
		}

		if (setMatcher.find()) {
			set = Optional.of(setMatcher.group(1).trim());
		}

		if (costMatcher.find()) {
			cost = Optional.of(costMatcher.group(1).trim());
		}

		if (baseFlairMatcher.find()) {
			baseFlair = Optional.of(baseFlairMatcher.group(1).trim());
		}

		if (evolvedFlairMatcher.find()) {
			evolvedFlair = Optional.of(evolvedFlairMatcher.group(1).trim());
		}

		OptionalInt evolvedStart = OptionalInt.empty();

		if (evolvedMatcher.find()) {
			evolvedStart = OptionalInt.of(evolvedMatcher.start());
		}

		if (statsMatcher.find()) {
			if (evolvedStart.isPresent() && statsMatcher.start() > evolvedStart.getAsInt()) {
				evolvedStats = Optional.of(statsMatcher.group(1).trim());
			} else {
				baseStats = Optional.of(statsMatcher.group(1).trim());
				if (statsMatcher.find()) {
					evolvedStats = Optional.of(statsMatcher.group(1).trim());
				}
			}
		}

		if (effectMatcher.find()) {
			if (evolvedStart.isPresent() && effectMatcher.start() > evolvedStart.getAsInt()) {
				evolvedEffect = Optional.of(effectMatcher.group(1).trim());
			} else {
				baseEffect = Optional.of(effectMatcher.group(1).trim());
				if (effectMatcher.find()) {
					evolvedEffect = Optional.of(effectMatcher.group(1).trim());
				}
			}
		}

		json.putOpt("craft", craft.orElse(null));
		json.putOpt("type", type.orElse(null));
		json.putOpt("rarity", rarity.orElse(null));
		json.putOpt("set", set.orElse(null));
		json.putOpt("cost", parseInt(cost).orElse(null));

		final JSONObject base = new JSONObject();
		final JSONObject evolved = new JSONObject();

		base.putOpt("flair", baseFlair.orElse(null));
		base.putOpt("effect", baseEffect.orElse(null));
		evolved.putOpt("flair", evolvedFlair.orElse(null));
		evolved.putOpt("effect", evolvedEffect.orElse(null));

		Optional<Integer> baseAtk = Optional.empty();
		Optional<Integer> baseDef = Optional.empty();
		Optional<Integer> evolvedAtk = Optional.empty();
		Optional<Integer> evolvedDef = Optional.empty();

		if (baseStats.isPresent() && baseStats.get().contains("/")) {
			final String[] explode = baseStats.get().split("/");
			baseAtk = parseInt(explode[0]);
			baseDef = parseInt(explode[1]);
		}

		if (evolvedStats.isPresent() && evolvedStats.get().contains("/")) {
			final String[] explode = evolvedStats.get().split("/");
			evolvedAtk = parseInt(explode[0]);
			evolvedDef = parseInt(explode[1]);
		}

		base.putOpt("atk", baseAtk.orElse(null));
		base.putOnce("def", baseDef.orElse(null));
		evolved.putOpt("atk", evolvedAtk.orElse(null));
		evolved.putOpt("def", evolvedDef.orElse(null));

		if (base.keySet().size() > 0) {
			json.put("base", base);
		}

		if (evolved.keySet().size() > 0) {
			json.put("evolved", evolved);
		}
	}

	private void downloadImages (@Nonnull final List<String> ids) {
		final Integer delay = opts.get(Options.delay);
		final File dest = getDest();
		assert delay != null;

		for (final String id : ids) {
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < 2; j++) {
					final String localName =
						String.format(
							"%s%s%s.jpg"
							, id, i == 1 ? "-evolved" : ""
							, j == 1 ? "-alt" : "");
					final File localFile = new File(dest, localName);
					if (localFile.exists()) {
						logger.info("Already have art for '%s', skipping...", id);
						continue;
					}
					final HttpGet get =
						new HttpGet(
							String.format("http://sv.bagoum.com/getRawImage/%d/%d/%s", i, j, id));
					for (int attempts = 0; attempts < 10; attempts++) {
						try {
							final HttpResponse res = client.execute(get);
							if (res.getStatusLine().getStatusCode() == 200) {
								logger.info("Downloading %s...", localName);
								final HttpEntity entity = res.getEntity();
								try (final OutputStream out = new FileOutputStream(localFile)) {
									entity.writeTo(out);
								}

								if (localFile.length() == 16) {
									logger.warn("%s did not exist...", localName);
									if (!localFile.delete()) {
										logger.warn(
											"Failed to clean up %s, delete manually..."
											, localName);
									}
								}
							} else {
								logger.warn("%s did not exist...", localName);
							}
							break;
						} catch (final IOException e) {
							logger.error("Failed to download %s, retrying...", localName);
						} finally {
							get.releaseConnection();
						}
					}
					try { Thread.sleep(delay); } catch (final InterruptedException ignored) {}
				}
			}
		}
	}

	@Nonnull
	private File getDest () {
		final File dest = opts.get(Options.dest);
		assert dest != null;

		if (!dest.exists() && !dest.mkdirs()) {
			return Options.DEFAULT_DEST;
		}

		return dest;
	}
}
